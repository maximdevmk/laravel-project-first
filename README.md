**Материал по работе с Laravel**
---
## Main command

1. php artisan serve - запуск сервера http://127.0.0.1:8000
2. php artisan make:seeder LinksTableSeeder - создание сида
3. php artisan make:model --factory Link - создание модели
4. php artisan make:migration create_links_table --create=links - создание миграции (создание таблицы)
5. php artisan migrate:fresh --seed - запуск миграции, сидов

Создание первого проекта (инструкция) [инструкция](https://laravel-news.com/your-first-laravel-application)